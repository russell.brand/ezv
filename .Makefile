## Russell specific
VENV=/Users/reallyrussell/PycharmProjects/environs/general/bin
ACTIVE=. $(VENV)/activate
PIP=$(ACTIVE); pip
PYTHON=$(ACTIVE); python

## For general 
## copy .Makefile to Makefile
## use either
## 1) edit VENV, or
## 2) remove ACTIVE, ie:
## PIP=pip
## PYTHON=python
## as appropriate to your environment
##


install:
	$(PIP) install -r requirements.txt

extract:
	$(PYTHON) extract.py

part2:
	$(PYTHON) part2.py 

simple: 

	$(PYTHON) simple.py  1 2 1312
commit:
	git commit -a

status:
	git status

push:
	git push
	git pull
