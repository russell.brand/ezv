# EZV -- San Mateo Easy Video Recordinging Find

EZV simplifies finding video records (DVDs & Blu Ray) in the San Mateo County Library System.

## The Project Description may be found as project 12 in:

https://docs.google.com/document/d/1PU8tpt9iH5d6ndyE9MAQxqw3W1e9l3C9SRHDcz-sy_w/edit?usp=sharing

## Reference Materials

https://www.thepythoncode.com/article/extracting-and-submitting-web-page-forms-in-python	

https://www.crummy.com/software/BeautifulSoup/bs4/doc/

## samples
	
### sample.html 

copy of the raw page that has the initial form on it

### listing.html

first page of results from querry.

This is a BIG gile without line breaks

## Instructions
### If you use Makefiles 

#### make your VENV if appropriate

#### copy & edit Makefile

    cp .Makefile Makefile

#### install the necessary packages

    make install

#### run the current version -- so far we are getting the first page of the listing

    make extract

    make part2

### if you are not using make

#### run the current version -- so far we are getting the first page of the listing

    python extract.py
	
	python part2.py



## Obvservations

### Since the the form on the first page does not use security token, we can bypass much of "part2.py" 

    https://smcl.bibliocommons.com/v2/search?query=star%20trek&searchType=title

### clicking the boxes will run javascript in annoying ways. 

To get write answers, we can instead

	https://smcl.bibliocommons.com/v2/search?query=star%20trek&searchType=title&f_FORMAT=DVD%7CBLURAY

which will so as "simple.py"

## TODO

### where I am 2022-03-14
* part2 deals with the return properly

soup.find(class_='cp-pagination-label') 

a div if something is found
NONE if it is not

* part3 does the command line parsing & binary printing


### simple.py
- [ ] parse results

-- using docopt

-- check how to use arv[0]

- [ ] submit second form (subselect to just DVDs and Blu Ray)
- [ ] retrieve parse results for all pages from second form
- [ ] format results for display
- [ ] read command line args

### makefile
- [ ] figure out how to to make the Makefile portable

will a symlink to VENV file do it
is there a way in emacs to do an active before running MAKE

### dev environment
- [ ] declarations
- [ ] linting
- [ ] check formatting of README
- [ ] tests

## DONE

- [x] extractor
- [x] submit first form (query by title)
