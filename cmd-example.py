#!/Users/reallyrussell/PycharmProjects/environs/general/bin/python

# copied from from
# https://realpython.com/comparing-python-command-line-parsing-libraries-argparse-docopt-click/#command-line-example


# In order to add an option, we add a <name> to the docstring. The <>
# are used to designate a positional argument. In order to execute the
# correct logic we must check if the command (treated as an argument)
# is True at runtime if arguments['hello']:, then call the correct
# function.

"""Greeter.

Usage:
  basic.py hello <name>
  basic.py goodbye <name>
  basic.py (-h | --help)

Options:
  -h --help     Show this screen.

"""
from docopt import docopt


def hello(name):
    print('Hello, {0}'.format(name))


def goodbye(name):
    print('Goodbye, {0}'.format(name))

if __name__ == '__main__':
    arguments = docopt(__doc__)

    # if an argument called hello was passed, execute the hello logic.
    if arguments['hello']:
        hello(arguments['<name>'])
    elif arguments['goodbye']:
        goodbye(arguments['<name>'])
