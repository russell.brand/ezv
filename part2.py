from urllib.parse import urljoin
from pprint import pprint

from extract import get_desired_form, get_form_details, session

from extract import URL

# will get TITLE from the command line in production
TIT = "star trek picard"

f=get_desired_form()
form_details = get_form_details(f)



data = {}

if True: 
    for input_tag in form_details["inputs"]:
        if input_tag['name'] ==  'q':
            data['q'] =  TIT
        elif input_tag['name'] ==  't':
            data['t'] =  'title'
        elif input_tag['name'] ==  'post_type':
            data['post_type'] = 1
        elif input_tag["type"] == "hidden":
            # if it's hidden, use the default value
            data[input_tag["name"]] = input_tag["value"]
        elif input_tag["type"] == "select":
            for i, option in enumerate(input_tag["values"], start=1):
                # iterate over available select options
                if option == input_tag["value"]:
                    print(f"{i} # {option} (default)")
                else:
                    print(f"{i} # {option}")
                    choice = input(f"Enter the option for the select field '{input_tag['name']}' (1-{i}): ")
            try:
                choice = int(choice)
            except:
                # choice invalid, take the default
                value = input_tag["value"]
            else:
                value = input_tag["values"][choice-1]
                data[input_tag["name"]] = value
        elif input_tag["type"] != "submit":
            # all others except submit, prompt the user to set it
            value = input(f"Enter the value of the field '{input_tag['name']}' (type: {input_tag['type']}): ")
            data[input_tag["name"]] = value

# Join The Url With the action (form request URL)
url = urljoin(URL, form_details["action"])
# pprint(data)
if form_details["method"] == "post":
    res = session.post(url, data=data)
elif form_details["method"] == "get":
    res = session.get(url, params=data)

pprint(res)
print("-" * 50)
pprint(res.reason)
print("-" * 50)
print(res.html)
print("-" * 50)
print(len(res.content))


soup = BeautifulSoup(res.html.html, "html.parser")
print(soup.find(class_='cp-pagination-label'))
