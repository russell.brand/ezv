Project 12: Warm up -- screen scraping problem

Our county library has it’s card catalog online

https://smcl.org/welcome-to-the-san-mateo-county-libraries/

Step 1

From the command line take a TITLE.

Do a search on that title and open the resulting page in a web browser.

Step 2

Restrict Search to DVDs & Blu Rays.

Step 3
Construct a web page locally showing 
All the results sort alphabetically by title
In the results only show title
One to a line
With links from that title

Step 4 
When there are identical titles, merge into a single entry
Subsort by availability 
If the piece is not currently available show holds / copies
Color code DVDs vs Blu Rays.

Step 5
Make the links open in a new tab
