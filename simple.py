import sys
from bs4 import BeautifulSoup
from requests_html import HTMLSession
from urllib.parse import urljoin

# queries will look like
# https://smcl.bibliocommons.com/v2/search?query=star%20trek&searchType=title&f_FORMAT=DVD%7CBLURAY&page=0

# 1 get the title from command line (for the moment we will use a constant)

# TITLE_DEFAULT : str  = "star trek"

TITLE_DEFAULT : str  = "star"

title: str

print("sys.argv=" ,sys.argv)
if len(sys.argv) == 1:
    title=TITLE_DEFAULT
else:
    title=" ".join(sys.argv[1:])

# ? do I need to quote the title"
print("title=",title)





#2  form initial query (with dvd & blue ray restricons)

# BASE_URL: str = "https://smcl.bibliocommons.com/v2/search?"

BASE_URL: str = "https://smcl.bibliocommons.com/v2/search"

query_args : str =  {
    "query" : title,
    "searchType" : "title",
    ## "D&f_FORMAT" : "DVD|CBLURAY",
    "page" : 0
    }




# 3 run initial query
session = HTMLSession()
res = session.get(BASE_URL, data=query_args)

# let's save the results to a file of a success and failur both in the
# normal browser and in system.
#
# Then diff them
#
# https://docopt.readthedocs.io/en/latest/



# # 4 if there are no results stop

# div class="cp-empty-search-result"

# ??  is the double HTML correct ??



soup = BeautifulSoup(res.html.html, "html.parser")
foo = soup.find(class_="cp-empty-search-result")

bar = soup.find(class_="cp-pagination-label")


# raise(NotImplementedError)

# #  count results

# raise(NotImplementedError)

# # loop results -> structure

# raise(NotImplementedError)

# # generate page from structure

# raise(NotImplementedError)

